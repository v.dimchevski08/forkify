import axios from 'axios'; 
// import { apiAppID, apiKey, baseURL, recipeURL } from '../config';
 
export default class Search {
    constructor(query){
        this.query = query;
    }
 
    async getResults() {
        try {
            const results = await axios(`https://forkify-api.herokuapp.com/api/search?&q=${this.query}`);
            this.recipes = results.data.recipes;
            // console.log(this.recipes);
        } catch(error) {
            alert(error);
        }
    }
}